all:

build:
	go build -v

# This is a dumb "test".  You need to go inspect the files created to
# see if they conform to what you think they should be!
manual-test:
	rm -rf tmp
	mkdir -p tmp
	cp zip1 tmp/
	( cd tmp && mv zip1 copy-of-zip1 && ../zip1 f.zip copy-of-zip1 )
	( cd tmp && mkdir unzippin && mv f.zip unzippin && cd unzippin && unzip f.zip )
	( cd tmp/unzippin && diff copy-of-zip1 ../copy-of-zip1 )

clean:
	rm -rf tmp zip1
