// The zip1 command creates a zipfile with a single file in it.
package main

import (
	"archive/zip"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"md0.org/fileutil"
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "zip1 creates a zipfile with one file in it\n")
		fmt.Fprintf(os.Stderr, "\n")
		fmt.Fprintf(os.Stderr, "usage: zip1 <zipfile> <file>\n")
		fmt.Fprintf(os.Stderr, "       zipfile must not exist\n")
		fmt.Fprintf(os.Stderr, "       file must exist\n")
		os.Exit(2)
	}

	flag.Parse()
	if flag.NArg() != 2 {
		flag.Usage()
	}

	var (
		zipfile = flag.Arg(0)
		file    = flag.Arg(1)
	)

	if fileutil.IsExist(zipfile) {
		fmt.Fprintf(os.Stderr, "ERROR: zipfile %s exists\n", zipfile)
		flag.Usage()
	}

	if !fileutil.IsExist(file) {
		fmt.Fprintf(os.Stderr, "ERROR: file doesn't exist: %s\n", file)
		flag.Usage()
	}

	if !fileutil.IsFile(file) {
		fmt.Fprintf(os.Stderr, "ERROR: %s isn't a file\n", file)
		flag.Usage()
	}

	err := z(file, zipfile)
	if err != nil {
		log.Fatal(err)
	}
}

// Help for this from (thanks!):
// https://gist.github.com/svett/424e6784facc0ba907ae
//
// Before this is called, we know target (the soon-to-be zipfile) does
// not exist and source (the file-to-be-added-to-the-zipfile) does
// exist.
func z(source, target string) error {
	zipfile, err := os.Create(target)
	if err != nil {
		return err
	}

	info, err := os.Stat(source)
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}
	header.Method = zip.Deflate
	// We let header.Name be the basename of the file; in this
	// world, no directories are allowed.

	archive := zip.NewWriter(zipfile)

	// https://golang.org/pkg/archive/zip/#Writer.CreateHeader
	w, err := archive.CreateHeader(header)
	if err != nil {
		return err
	}

	content, err := ioutil.ReadFile(source)
	if err != nil {
		return err
	}

	// Instead of a chain of `if err!=nil {return nil}`, this closes
	// archive and zipfile no matter what (I think?), but prefers
	// returning the earliest of the three possible errors (in
	// order: from w.Write, archive.Close, zipfile.Close).  This
	// pattern (and not defering zipfile.Close much earlier) is
	// based on GOPL, p. 148.
	_, err = w.Write(content)
	if cerr := archive.Close(); err == nil {
		err = cerr
	}
	if cerr := zipfile.Close(); err == nil {
		err = cerr
	}
	return err
}
