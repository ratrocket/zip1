# zip1

> **(May 2021)** moved to [md0.org/zip1](https://md0.org/zip1).

The zip1 command creates a zipfile with one file in it.  No directories
allowed!  (It's purposely that limited.)
